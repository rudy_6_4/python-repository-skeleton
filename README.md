# README #

## What is this repository for? ##

This repository proposes a pseudo-template for python projects.
It provides the following functionalities:

* environment setup (using `conda`, `pip`, `pip-tools`)
* reproducibility via version locking (using `pip-compile-multi`, `pip-tools`)
* code convention and good practices (using `black`, `isort`, `pylint`)
* code correctness (using `pylint`, `pytype`)
* tests and coverage (using `pytest`)
* documentation presence (using `pylint`)
* integration with bitbucket pipelines (using `bitbucket-pipelines`, `scripts/`)
* integration with developer (using `scripts/`)

It is **linux** and **python3**.

<details><summary>Why `conda` ?</summary>
<p> `conda create` helps to have any python version directly in a virtual environment independent from the base system. It can also helps with non python dependencies needed. </p>
</details>

<details><summary>Why `pip-tools` and `pip-compile-multi` ?</summary>
<p> `pip-compile` has a dependency solver contrary to `pip` and generate lock files. </p>
<p> `pip-compile-multi` is a helper that manage many lock files generation and verification. </p>
<p> `pip-sync` is equivalent to `pip install` but it also no longer needed dependencies. </p>
<p> Alternatives are slower and less flexible: `poetry`, `pyflow`, `pipenv`. </p>
</details>

<details><summary>Why `pytype` ?</summary>
<p> `pytype` does type checking based on annotation but also full type inference. </p>
<p> Alternatives does only local type inference: `mypy`, `pyre`, `pyright`. </p>
</details>


## How do I get set up my development python environment ? ##

----------------------------------------------------------
**NOTE**:
All commands are to be run from the root of the git repository.
----------------------------------------------------------

### Specify your project identity

First edit `pyproject.toml` to match your project (name, email ...).
Rename the *my_package* repository to match the name of the package or the project:

`git mv my_package package_name`

### Specify/Update its dependencies

The python interpreter version can be selected in `pyproject.toml`, in section `[conda]`.

Update the requirements directory, it contains:

- abstract dependencies in `.in` files, e.g. `dev.in` and `base.in`, that you can edit.
- concrete dependencies, i.e. with versions locked, in `.txt` files, that you should never edit manually.

To lock the all the dependencies including the transitive ones, run:

`scripts/lock_dependencies_versions.sh`

It will create/update `dev.txt` and `base.txt`.
These files are the key to the reproducibility of build and the continuous integration pipeline.
You should not never edit them manually.

To update only the version of a given dependency:

`venv/dev/bin pip-compile-multi -u -P dependency_package`

where `venv/dev` is your development environment. For more complex operation [read the doc](https://pip-compile-multi.readthedocs.io/en/latest/features.html).

### Create a fresh development environment

Run `scripts/install_devenv.sh`.
It will create a python environment in `venvs/devenv` with all needed dependencies.

If you want to install in a given environment, run `pip install -r requirements/dev.txt` inside.

### Run the package server

----------------------------------------------------------
**NOTE**:
You need a development environment.
----------------------------------------------------------

Run `scripts/test_my_package_server.sh`.
It should open a browser/tab with a browsable API.


### TODO

- Publish doc
- Publish test coverage
- Python packaging
- Test after real package installation
