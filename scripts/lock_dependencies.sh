#!/bin/bash -e

# Install devenv
# Generate or update requirements/*.txt

source scripts/env.sh

scripts/install_python_in_devenv.sh

PMC=pip-compile-multi

if [ ! -x "$(command -v $PMC)" ]
then
  PMC_VERSION=$(grep -ho "$PMC==[0-9a-z.]\+" requirements/*)
  pip install $PMC_VERSION -c $DEVLOCK
fi

ADD_HASHES=$(find requirements -name '*.in' -printf ' -g %f ' | sed 's/\.in //g')
$PMC -u --no-upgrade $ADD_HASHES
