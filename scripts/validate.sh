#!/bin/bash -e

source scripts/env.sh

SRCDIRS="$(dirname */*.py | uniq)"
ALL_PY="$(find $SRCDIRS -name '*.py')"

if ! pip-compile-multi verify
then
  scripts/lock_dependencies.sh
  scripts/install_dependencies_in_devenv.sh
fi

black -q $SRCDIRS

time pylint $ALL_PY

isort --check-only --quiet --diff $ALL_PY

pydocstyle --config pydocstyle.ini $SRCDIRS

if [ -x "$(command -v pytype)" ]
then
  time pytype $ALL_PY
fi

pytest \
  --junitxml=test-reports/report.xml \
  --cov-report=html:test-reports/coverage.html \
  tests
