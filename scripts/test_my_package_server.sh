# Use https://github.com/tiangolo/uvicorn-gunicorn-docker/tree/master/docker-images scripts for production

source scripts/env.sh

(sleep 1; sensible-browser http://127.0.0.1:8000/docs/) &

$DEVENV/bin/uvicorn my_package.main:app --reload
