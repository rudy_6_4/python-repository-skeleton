#!/bin/bash -e

source scripts/env.sh

# keep environment updated WRT DEVLOCK and clean
$DEVENV/bin/pip-sync $DEVLOCK || $DEVENV/bin/pip install -r $DEVLOCK
# as pip-sync is not available the first time, we rely on pip

# check that requirements versions are up to date (i.e. requirements/*.txt)
$DEVENV/bin/pip-compile-multi verify
