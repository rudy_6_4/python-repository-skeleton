#!/bin/bash -e

PYVERSION=$(sed -n -e 's/python = "\(.\+\)".*/\1/p' pyproject.toml)
BASE=$PWD
ENVS=$BASE/venvs
DEVENV=$ENVS/dev
MINICONDA=$HOME/miniconda
export PATH=$DEVENV/bin:$MINICONDA/bin:$PATH
if [ -x "$(command -v g++)" ]
then
  DEVLOCK=requirements/dev-if-g++.txt
else
  DEVLOCK=requirements/dev.txt
fi


export PYTHONPYCACHEPREFIX=$DEVENV

SHORT_PATH="realpath --relative-to=."
