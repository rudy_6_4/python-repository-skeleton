#!/bin/bash -e

source scripts/env.sh

# if devenv not here or with the wrong version of python
if [ ! "$($DEVENV/bin/python --version)" == "Python $PYVERSION" ]
then
  # check conda is here
  if [ ! -x "$(command -v conda)" ]
  then
    wget -c https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /tmp/miniconda.sh
    bash /tmp/miniconda.sh -b -u -p $MINICONDA
  fi
  #PKGS="python=$PYVERSION -c conda-forge 'gcc_linux-64'"
  PKGS="python=$PYVERSION"
  conda create --copy --yes -q -p $DEVENV $PKGS  || conda update -p $DEVENV $PKGS || echo "Please clean $DEVENV"
fi
