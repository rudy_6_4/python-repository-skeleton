"""Contains tools for CPU-Bound execution."""
from asyncio import get_event_loop
from concurrent.futures import ProcessPoolExecutor
from functools import wraps


def async_decorator_in_created_process_pool(max_workers):
    """Create a decorator function to executes in a multi-process executor pool.

    Arguments:
    max_workers -- maximum nb of process in the pool

    Return:
    Decorator function.
    Due to a pickle bug it can be used with decorator syntax (e.g. @).
    """
    executor = ProcessPoolExecutor(max_workers)

    def in_async_pool(function):
        """Decorate a function to executes in the global multi-process executor pool."""

        @wraps(function)
        def run(*args, **kwargs):
            run_in_executor = get_event_loop().run_in_executor
            return run_in_executor(executor, function, *args, **kwargs)

        return run

    return in_async_pool
