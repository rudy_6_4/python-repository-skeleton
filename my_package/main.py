"""Example API."""
from typing import List

import numpy as np
from fastapi import FastAPI

from my_package.analytics import maximum
from my_package.cpu_bound import async_decorator_in_created_process_pool

app = FastAPI()

# cpu-bound functions will be executed in a pool with this decorator
parrallel_async = async_decorator_in_created_process_pool(max_workers=4)


@app.get("/")
def read_root():
    """Say hello."""
    return {"Hello": "World"}


@app.post("/threshold/upper")
async def upper_threshold(signal: List[float], maximum_cross_rate=0.999) -> float:
    """Compute an upper threshold that should not be crossed more than a given rate.

    Arguments:
    signal -- real values list
    maximum_cross_rate -- the rate that a similar signal cross the threshold

    Return:
    the upper threshold
    """
    _ = maximum_cross_rate
    array = np.array(signal)
    async_maximum = parrallel_async(maximum)
    threshold = await async_maximum(array)
    return threshold
